<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Forum</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
<script src="/rex/js/common.js"></script>
</head>

<body>
<div data-role="page" id="forum_view" data-add-back-btn="true">
    <div data-role="header">
		<a id="backButton" href="" data-transition="slide" data-direction="reverse" data-rel="back">Back</a>
   	    <h1>Forum Topic</h1>
<!--        <div data-role="navbar">
            <ul>
                <li><a href="#" class="ui-btn-active">View</a></li>
                <li><a id="editButton" href="#">Edit</a></li>
            </ul>
        </div>-->
    <a id="replyButton" href="#replyPopup" data-rel="popup" data-position-to="window" data-transition="pop" class="ui-btn ui-btn-icon-left ui-icon-comment ui-shadow ui-corner-all">Reply</a>
    </div>
    
	<div data-role="content">
        <fieldset class="ui-grid-a">
        	<div class="ui-block-a"><h2 id="topic">Topic</h2></div>
    		<div class="ui-block-b" style="text-align:right"><a id="actionButton" href="#actionPopup" data-rel="popup" data-transition="slideup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-action ui-btn-icon-left ui-btn-a">Actions</a></div>
        
        </fieldset>
        
        <div id="ui-body-test" class="ui-body ui-body-a ui-corner-all" style="margin-bottom:1em;">
        	<p id="content">content</p>
            <p style="text-align:right"><strong style="margin-right:10px;" id="nickname">UserA</strong>(<span id="date">2016-1-1</span>)<img id="icon" src="/rex/facebook/image.php?userId=0" class="ui-corner-all" style="margin-top:12px; margin-left:12px;"></p>
		</div>
<!--        <div><a class="ui-shadow ui-btn ui-corner-all" id="chnagePassword" href="chnagePassword">Reply</a></div>              
-->
        <!--<p id="tag"><strong>Tag:</strong> <a>#iphone</a> <a>#ios</a> <a>#android</a></p>-->
        <!--<p id="timestamp" style="text-align:right;"><strong>(Last Edit: 2000-0-0)</strong></p>-->
        <div style="margin-bottom:1em;">
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
                <ul id="replyListView" data-role="listview" data-theme="a" data-inset="true">
                    <li class="ui-grid-a"><div class="ui-block-b" style="font-size: .9em; width:60%; line-height:30px; word-wrap: break-word; white-space:normal;">This is goods xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</div>
                    <div  class="ui-block-c" style="text-align:right;margin-right: 0; width:40%; "><a href="#replyPopup" data-rel="popup" data-transition="slideup" class="ui-btn ui-corner-all ui-shadow ui-icon-comment ui-btn-inline ui-btn-icon-left">Include</a></div>
                    <p style="width:100%; text-align:right;"><strong>User2</strong> 2011-2-22 #2</p>
                    </li>
                </ul>
    	</div>
    </div>
    
    <div data-role="footer" data-position="fixed">
      <div data-role="navbar">
          <ul>
            <li><a href="#" class="ui-btn-active homeButton" data-icon="home">Home</a></li>
            <li><a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="arrow-u">Login</a></li>
            <li><a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="plus">Register</a></li>
          </ul>
      </div>
	</div>
    <div data-role="popup" id="replyPopup" data-theme="a" class="ui-corner-all" data-history="false">
            <div style="padding:10px 20px;">
                <h3>Reply</h3>
                <input type="hidden" id="replyIncludeId" value="0"/>
                <label for="un" class="ui-hidden-accessible">Comment:</label>
                <textarea rows="20" cols="40" id="replyCommentText" placeholder="Comment" data-theme="a"></textarea>
                <button id="replySubmit" type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-icon-left ui-icon-check">Submit</button>
            </div>
    </div>
    <div data-role="popup" id="actionPopup" data-theme="b" data-history="false">
            <ul data-role="listview" data-inset="true" style="min-width:210px;">
                <li data-role="list-divider">Choose an action</li>
                <li><a id="editButton" href="#editPopup">Edit</a></li>
                <li><a id="deleteButton" href="#deletePopup">Delete</a></li>
                <!--<li><a href="#deletePopup">Attachment</a></li>-->
            </ul>
    </div>
  	<div data-role="footer" data-position="fixed" class="ui-footer ui-bar-inherit ui-footer-fixed slideup">
    	<div data-role="navbar">
            <ul>
          		<li><a id="homeButton" href="/rex/" class="ui-btn-active" data-icon="home">Home</a></li>
          		<li><a id="centerButton" href="login" data-position-to="window" data-icon="arrow-u" data-transition="slideup">Login</a></li>
           		<li><a id="rightButton" href="register" data-position-to="window" data-icon="plus registerButton" data-transition="pop">Register</a></li>
            </ul>
		</div>
    </div>

<script>
$(document).ready(function(e) {
	var topicId = <?php echo $_GET["topicId"]; ?>;
	var actionClickIndex = 0;
	
	// check 
	$.getJSON("/rex/program/forum/isTopicExist/" + topicId, {}, function(obj){
		if(obj['result'] == false){
			alert("Topic not exist!");
			localStorage.removeItem("redirectUrl");
			return;
		}else{
			console.log("Topic exist, OK");	
		}
	});

	if(localStorage.getItem("login") == null ){
		$("#forum_view #replyButton").hide();
	}

	$("#forum_view #backButton").attr("href", rootPath + "/forum");
	$("#forum_view #editButton").attr("href", rootPath + "/forum/view/"+ topicId +"/edit");

	$.getJSON(rootPath + "/program/forum/view/" + topicId, function(obj){
		if(obj['result']){
			console.log("user id="+obj['userId']);
			printLog(JSON.stringify(obj), 'view');
			$("#forum_view #topic").html(obj["topic"]);
			$("#forum_view #content").html(obj["content"]);
			$("#forum_view #nickname").html(obj["nickName"]);
			$("#forum_view #date").html(obj["date"]);
			$("#forum_view #icon").attr("src","/rex/facebook/image.php?userId="+obj['userId']);
			$("#forum_view #actionButton").hide();
			if(obj["canEdit"] == true){
				$("#forum_view #actionButton").show();
			}
		}else{
			$.mobile.changePage( rootPath + '');	
		}
	});
	
	// on click reply submit button
	$('#forum_view #replyPopup #replySubmit').click(function(){
		
		
		$('#forum_view #replyIncludeId').val(0);
		var includeReplyId = 0;
		var comment = $('#forum_view #replyCommentText').val();
		
		if(comment == ""){
			alert("Comment can't tempty!");
			return;
		}
		
		$.post( rootPath + "/program/forum/reply", {topicId:topicId, comment:comment, includeReplyId:includeReplyId}, function(msg){
			printLog(msg);
			var obj = JSON.parse(msg);
			if(obj['result'] == true){
				printLog('Reply OK');
				//window.location.replace( rootPath + '/forum/view/' + topicId);
				//$.mobile.changePage( rootPath + '/forum/view/' + topicId);
				//location.reload();
				
				var userId = localStorage.getItem("login").split(":")[3];
				var nickName = localStorage.getItem("login").split(":")[2];
				var count = $('#forum_view #replyListView li').length +1;
				$('#forum_view #replyListView').prepend('<li class="ui-grid-a ui-li-static ui-body-inherit ui-first-child ui-last-child"><div class="ui-block-b" style="font-size: .9em; width:60%; line-height:30px; word-wrap: break-word; white-space:normal;">'+comment+'</div><div class="ui-block-c" style="text-align:right;margin-right: 0; width:40%; "></div><p style="width:100%; text-align:right;"><strong>'+nickName+'</strong> (Today) #'+count+'<img src="/rex/facebook/image.php?userId='+userId+'" class="ui-corner-all" style="margin-top:12px; margin-left:12px;"></p></li>');
				$('#forum_view #replyListView').listview('refresh');
			}else{
				alert('reply error');	
			}
		});
		
		$('#forum_view #replyPopup').popup("close");
	});
	
	
	// get all json of reply
	$.getJSON( rootPath + "/program/forum/view/"+topicId+"/allReply", {topicId:topicId}, function(obj){
		printLog(JSON.stringify(obj), 'reply');
		$('#forum_view #replyListView').html("");
		
		if(obj['result'] == true){
			$('#forum_view #replyListView').html("");
			$.each(obj['reply'], function(index, value){
				var total = obj['reply'].length;
				var date = value['date'];
				if(value['selfReply'] == true){
					console.log("me");
					$('#forum_view #replyListView').append('<li class="ui-grid-a"><input type="hidden" value="'+value['replyId']+'"><div class="ui-block-b" style="font-size: .9em; width:60%; line-height:30px; word-wrap: break-word; white-space:normal;">'+value['comment']+'</div><div class="ui-block-c" style="text-align:right;margin-right: 0; width:40%; "><a  id="replyactionButton" href="#actionPopup" data-rel="popup" data-transition="slideup" class="ui-btn ui-corner-all ui-shadow ui-icon-action ui-btn-inline ui-btn-icon-left">Actions</a></div><p style="width:100%; text-align:right;"><strong>'+value['nickName']+'</strong> ('+date+') #'+(total-index)+'<img src="/rex/facebook/image.php?userId='+value['userId']+'" class="ui-corner-all" style="margin-top:12px; margin-left:12px;"></p></li>');
				}else{
					console.log("not me");
					$('#forum_view #replyListView').append('<li class="ui-grid-a"><div class="ui-block-b" style="font-size: .9em; width:60%; line-height:30px; word-wrap: break-word; white-space:normal;">'+value['comment']+'</div><div class="ui-block-c" style="text-align:right;margin-right: 0; width:40%; "></div><p style="width:100%; text-align:right;"><strong>'+value['nickName']+'</strong> (2011-2-22) #'+(total-index)+'<img src="/rex/facebook/image.php?userId='+value['userId']+'" class="ui-corner-all" style="margin-top:12px; margin-left:12px;"></p></li>');
				}
			});
			$('#forum_view #replyListView').listview('refresh');
		}
		
		onClickReplyButton();
	});
	
	// action popup click edit
	/*$('#forum_view #actionPopup a').click(function(){
		var clickIndex = $(this).parent().index();
		if(clickIndex == 1){
			printLog('edit reply');	
			//window.location.href = '';
			$.mobile.changePage( rootPath +'/forum/view/'+topicId+'/edit' );
		}
		
	});*/
	
	var actionReplyId = -1;
	var clickDetailIndex = -1;
	function onClickReplyButton(){
		$('#forum_view #replyListView #replyactionButton').click(function(){
			printLog('click reply action buttn');
			clickDetailIndex = $(this).parent().parent().index();
			printLog(clickDetailIndex, 'clickDetailIndex');
			var replyId = $(this).parent().parent().find('input').val();
			actionReplyId = replyId;
			printLog(replyId, 'replyId');
			$('#forum_view #actionPopup #editButton').attr('href', rootPath + '/forum/reply/' + replyId + '/edit');

		});
	}
	
	// main action button
	$('#forum_view #actionButton').click(function(){
		actionReplyId = -1;
		clickDetailIndex = -1;
		printLog('action button');
		$('#forum_view #actionPopup #editButton').attr('href', rootPath + '/forum/view/' + topicId + '/edit');
	});
	
	
	// Delete part
	$('#forum_view #actionPopup #deleteButton').click(function(){
		printLog(actionReplyId,'reply id');
		var type = 'topic';
		var url = rootPath + "/program/forum/deleteTopic";
		
		// delete reply
		if(actionReplyId != -1){
			type = 'reply';
			url = rootPath + "/program/forum/deleteReply";
			
			var replyId = actionReplyId;
			$.ajax({
				url: url,
				data: {replyId: replyId},
				type: "DELETE",
				dataType:'text',
			
				success: function(msg){
					printLog("OK, " + msg);
					var obj = JSON.parse(msg);
					if(obj['result'] == true){
						$('#forum_view #replyListView li').eq(clickDetailIndex).detach();
					}
				},
			});
			
		// delete topic
		}else{
			type = 'topic';
			url = rootPath + "/program/forum/deleteTopic";
			
			$.ajax({
				url: url,
				data: {topicId:topicId},
				type: "DELETE",
				dataType:'text',
			
				success: function(msg){
					printLog("OK, " + msg);
					var obj = JSON.parse(msg);
					if(obj['result'] == true){
						window.location.replace(rootPath + '/forum');	
					}
				},
			});
		}
		
		
	});

});
</script>
</div>

</body>
</html>
