var app = playground({
	// Gobal Setting
	width: 750,
	height: 1334,
	preferedAudioFormat: "mp3",
	paths: {
		images: "gamefiles/images/",
		sounds: "gamefiles/sounds/"
	},
	// Loading
	create: function() {
		// Setting
		this.mainCharacterPos = {x:2, y:2};
		this.actionPointMax = 20;
		this.thiefNum = 3;
		this.policeNum = 5;
		this.caseNum = 8;
		this.apNum = 4;
		this.cwNum = 3;
		this.changeWorldChance = 1;

		// State
		this.isReadmePageVisible = true;
		this.isGameOver = false;

		// Loading images
		this.loadImage("bg");
		this.loadImage("ship");
		this.loadImage("thief");
		this.loadImage("mainCharacter");
		this.loadImage("police");
		this.loadImage("case");
		this.loadImage("ap");
		this.loadImage("cw");
		this.loadImage("actionPointBar");
		//this.loadImage("actionPointBarBg");
		this.loadImage("ground");
		this.loadImage("rotateCircle");
		this.loadImage("itemBg");
		//	[Readme]
		this.loadImage("readmeButton");
		this.loadImage("readmePage");
		this.loadImage("readmePageCloseButton");

		// Loading sound
		this.loadSounds("bgm","button65");

		//this.thomas = {
		//	x: 0,
		//	y: this.center.y,
		//	width: 128,
		//	height: 128,
		//	color: "#e2543e",
		//	speed: 100
		//};

		// create bg card
		this.cardsBasePos = {x:38, y:this.center.y-(120+20)*5/2+(20/2)};
		this.cards = [];
		var cardCount = 0;
		for( var i=0; i<5; i++){
			var cardRow = [];
			for (var j = 0; j <5; j++) {
				var tempCard = {
					width: 120,
					height: 120,
					x: (120+20)*j +this.cardsBasePos.x,
					y: (120+20)*i +this.cardsBasePos.y,
					index: cardCount,
					name: 'card_'+(cardCount),
					hold: ""
				};
				cardRow.push(tempCard);
				cardCount++;
			}
			this.cards.push(cardRow);
		}
		//console.log(JSON.stringify(this.cards));
		//;

		// create main character
		this.mainCharacter = {
			width: 100,
			height: 100,
			//x: this.center.x - 100/2 + (this.mainCharacterPos.x * 120+20) - (2 * 120+20),
			//y: this.center.y - 100/2 + (this.mainCharacterPos.y * 120+20) - (2 * 120+20),
			x: this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].x + 10,
			y: this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].y + 10,
			type: "player",
			case: 0
		};
		this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].hold = this.mainCharacter;


		// create rotate circle
		this.resetRotateCircle();

		// create exit button
		this.exitButton = {
			width: 120,
			height: 60,
			x: this.width - 120 -20,
			y: 20,
			type: "button"
		};

		this.actionPoint = this.actionPointMax;
		// action point bar
		this.actionPotinBar = {
			widthMax:this.width - this.width/10,
			width: this.width - this.width/10,
			height: 40,
			x: this.width/20,
			y: this.height/5
		};


		// thief
		this.thief = [];
		for (var i=0; i<this.thiefNum; i++) {
			var tempThiefObj = {
				width: 120,
				height: 120,
				x: 0,
				y: 0,
				type: "thief",
				isAcitve: true
			};
			this.thief.push(tempThiefObj);

			this.holdGround(this.cards, this.thief[i]);
		}

		// police
		this.police = [];
		for (var i=0; i<this.policeNum; i++){
			var tempPoliceObj = {
				width: 120,
				height: 120,
				x: 0,
				y: 0,
				type: "police",
				isAcitve: true
			};
			this.police.push(tempPoliceObj);

			this.holdGround(this.cards, this.police[i]);
		}
		// case
		this.case = [];
		for (var i=0; i<this.caseNum; i++) {
			var caseType = parseInt(Math.random()*3);
			console.log("caseType = "+ caseType);
			var tempCaseObj = {
				width: 120,
				height: 120,
				x: 0,
				y: 0,
				type: "case",
				caesType: caseType,
				isAcitve: true
			};
			this.case.push(tempCaseObj);

			this.holdGround(this.cards, this.case[i]);
		}

		// ap
		this.ap = [];
		for (var i=0; i<this.apNum; i++) {
			var tempApObj = {
				width: 120,
				height: 120,
				x: 0,
				y: 0,
				type: "ap",
				isAcitve: true
			};
			this.ap.push(tempApObj);

			this.holdGround(this.cards, this.ap[i]);
		}

		// CW
		this.cw = [];
		for (var i=0; i<this.cwNum; i++) {
			var tempCwObj = {
				width: 120,
				height: 120,
				x: 0,
				y: 0,
				type: "cw",
				isAcitve: true
			};
			this.cw.push(tempCwObj);

			this.holdGround(this.cards, this.cw[i]);
		}

		// change world button
		this.changeWorldButton = {
			x: this.width / 8 ,
			y: this.height - this.height / 5.8 +4 ,
			width:120,
			height:120,
			isVisible: false
		};

		// change world button bg
		this.itemBg = {
			x: this.width / 8 -20 ,
			y: this.height - this.height / 5.8 -20 ,
			width:160,
			height:160
		};

		// [Readme Page]
		this.readmeButton = { x: this.width - this.width / 3.6, y: this.height - this.height / 5.8 +6, width:120,height:120 };
		this.readmePage = { x: 0, y: 0, width: this.width, height: this.height };
		this.readmePageCloseButton = { x: this.center.x - 320/2, y: this.height - 120/2-140, width: 320, height: 76 };
		// End [Readme Page]

		//this.tween(this.rotateCircle)
        //
		//	/* card animation */
        //
		//	.to({ rotation: -0.3 }, 0.5, "01")
		//	.to({ x: 400, y: 240, rotation: 0, scale: 1.0 }).wait(1.5)
		//	.to({ x: 400, y: 300, scale: 0.5 }, 0.5, "outBounce").wait(2)
		//	.to({ x: 400, y: 540, scale: 0.6 }, 1).wait(1)
        //
		//	/* make it go forever */
        //
		//	.loop();
		this.scoreFont = {
			size: 40,
			x: 20,
			text: 100
		};


	},
	changeWorld:function(){

		// create new ground, all empty string
		this.newGround = [];
		for (var i=0; i<5; i++){
			var rowNewGround = [];
			for (var j=0; j<5; j++){
				var tempNewGround = {
					width: 120,
					height: 120,
					x: (120+20)*j +this.cardsBasePos.x,
					y: (120+20)*i +this.cardsBasePos.y,
					hold: ""
				};
				rowNewGround.push(tempNewGround);
			}
			this.newGround.push(rowNewGround);
		}
		// push main character
		this.newGround[this.mainCharacterPos.y][this.mainCharacterPos.x].hold = this.mainCharacter;
		//console.log("[changeWorld] newGround="+JSON.stringify(this.newGround));
		//update pos
		for (var i=0; i<this.policeNum; i++){
			this.holdGround(this.newGround,this.police[i]);
		}
		for (var i=0; i<this.thiefNum; i++){
			this.holdGround(this.newGround,this.thief[i]);
		}
		for (var i=0; i<this.caseNum; i++){
			this.holdGround(this.newGround,this.case[i]);
		}
		for (var i=0; i<this.apNum; i++){
			this.holdGround(this.newGround,this.ap[i]);
		}
		for (var i=0; i<this.cwNum; i++){
			this.holdGround(this.newGround,this.cw[i]);
		}
		//this.cards = this.newGround;

		//clear
		for (var i=0; i<5; i++){
			for (var j=0; j<5; j++) {
				this.cards[i][j].hold = "";
			}
		}

		for (var i=0; i<5; i++){
			for (var j=0; j<5; j++) {
				this.cards[i][j] = this.newGround[i][j];
			}
		}
	},
	holdGround: function(ground, obj){
		if(obj.isAcitve == false){
			return;
		}

		var x = 0;
		var y = 0;
		do{
			x = parseInt(Math.random()*5);
			y = parseInt(Math.random()*5);
			//console.log('rand police x'+x+" y"+y);
		}while(ground[y][x].hold != "" || (x==this.mainCharacterPos.x && y==this.mainCharacter.y) );
		// if not empty or meet player random again

		//update
		ground[y][x].hold = obj;
		//console.log("!add police to card x:"+(x)+" y:"+(y));
		obj.x = ground[y][x].x;
		obj.y = ground[y][x].y;
	},

	resetRotateCircle: function(){
		this.rotateCircle = {
			width:160,
			height:160,
			scale: 1.4,
			x: this.center.x,
			y: this.height - 160/2-80,
			rotation: 0
		}
	},
	// Start
	ready: function() {
		//this.setState(ENGINE);

		// score
		this.score = 100;

		// text
		this.text = "Rex Web Assignment 2016";

		this.debugText = "(Poon Chun Sing 147373791)";

		this.actionPotinBarText = "Action Point";

		// music
		var bgm = app.sound.play("bgm", true);
		app.sound.setVolume(bgm, 0.5);
	},

	resize: function(width, height) {
		// find scale size
		var transformInfo = this.layer.canvas.style.transform;
		var result = transformInfo.match(/scale\([0-9]\.[0-9].+\)/i).toString(); // igonre case
		result = result.substring(6, result.length-1);
		var resultArr = result.split(',');
		this.scale = parseFloat(resultArr[0]);

		console.log('scale = ' + this.scale);
	},

	// Input
	mousedown: function(data) {
		this.inputProcess(data);

	},
	touchend: function(event) {
		var data = {x:event.x, y:event.y};
		this.inputProcess(data);
	},
	keydown: function(event) {
		if(event.key == "k"){
			this.changeWorld();
		}
		if(event.key == "j"){
			this.changeWorldChance += 1;
		}
		//this.text = event.key;  /* pressed key name */

	},
	inputProcess: function(data){
		this.trackIndex += 1;
		var newInputData = data;
		//this.text = "mouse down " + data.button + " " + data.x +  " , " + data.y;
		console.log('x='+data.x+" y="+data.y);

		//if(isClick(this.thomas,{x: data.x, y: data.y})){
		//	this.text = "click thomas , x ="+ data.x + ", y="+data.y;
		//	console.log("click");
		//}else{
		//	this.text = "";
        //
		//}

		// all cards
		for(var i=0; i< 5;i++){
			for (var j=0; j< 5;j++){
				isClick(this.cards[i][j], data, function(index){
					console.log('You click card '+index);
				});
			}
		}

		var that = this;
		// click rotate circle
		if(isClickByCenter(this.rotateCircle, data, function(){})){
			// can't press if readme page is shown
			if(this.isReadmePageVisible == false) {

				// press button music
				var button65 = app.sound.play("button65");
				//app.sound.setVolume(button65, 0.5);
				that.resetRotateCircle();

				var newPos = {x: this.mainCharacterPos.x, y: this.mainCharacterPos.y, tryCount: 0};
				var rotateAngle = 0;
				do {
					newPos.x = this.mainCharacterPos.x;
					newPos.y = this.mainCharacterPos.y;
					this.randResult = parseInt(Math.random() * 8);
					//console.log('Clicked rotate circle = '+ (-3.14*2 * this.randResult/7));

					rotateAngle = 0;

					//update roatateAngle
					switch (this.randResult) {
						case 0:
							break;
						case 1:
							rotateAngle = -(3.14 * 1 / 4);
							break;
						case 2:
							rotateAngle = -(3.14 * 1 / 2);
							break;
						case 3:
							rotateAngle = -(3.14 * 3 / 4);
							break;
						case 4:
							rotateAngle = -(3.14);
							break;
						case 5:
							rotateAngle = -(3.14 * 2 - 3.14 * 3 / 4);
							break;
						case 6:
							rotateAngle = -(3.14 * 2 - 3.14 * 1 / 2);
							break;
						case 7:
							rotateAngle = -(3.14 * 2 - 3.14 * 1 / 4);
							break;
					}


					//// save previous position
					//var tempMainCharacterPos = {}; // wrong tempMainCharacterPos = this.mainCharacterPos;
					//tempMainCharacterPos.x = this.mainCharacterPos.x;
					//tempMainCharacterPos.y = this.mainCharacterPos.y;


					// try new position
					switch (this.randResult) {
						case 0:
							newPos.y -= 1;
							console.log("上");
							break;
						case 1:
							newPos.x -= 1;
							newPos.y -= 1;
							rotateAngle = -(3.14 * 1 / 4);
							console.log("左上");
							break;
						case 2:
							newPos.x -= 1;
							rotateAngle = -(3.14 * 1 / 2);
							console.log("左");
							break;
						case 3:
							newPos.x -= 1;
							newPos.y += 1;
							rotateAngle = -(3.14 * 3 / 4);
							console.log("左下");
							break;
						case 4:
							newPos.y += 1;
							console.log("下");
							rotateAngle = -(3.14);
							break;
						case 5:
							newPos.x += 1;
							newPos.y += 1;
							rotateAngle = -(3.14 * 2 - 3.14 * 3 / 4);
							console.log("右下");
							break;
						case 6:
							newPos.x += 1;
							rotateAngle = -(3.14 * 2 - 3.14 * 1 / 2);
							console.log("右");
							break;
						case 7:
							newPos.x += 1;
							newPos.y -= 1;
							rotateAngle = -(3.14 * 2 - 3.14 * 1 / 4);
							console.log("右上");
							break;
					}

					//// must inside grids
					//if(this.mainCharacterPos.x>=0 && this.mainCharacterPos.x <=4 && this.mainCharacterPos.y>=0 && this.mainCharacterPos.y <=4){
					//	// main character animation
					//	this.tween(this.mainCharacter).wait(1).to(
					//	{x: this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].x + 10,
					//	y: this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].y + 10}
					//	,1).wait(1);
					//
					//}else{
					//	// recover position
					//	this.mainCharacterPos.x = tempMainCharacterPos.x;
					//	this.mainCharacterPos.y = tempMainCharacterPos.y;
					//	console.log("out!!");
					//}

					newPos.tryCount++;

				} while (!(newPos.x >= 0 && newPos.x <= 4 && newPos.y >= 0 && newPos.y <= 4));

				// del old position card hold
				this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].hold = "";

				// move to new position
				this.mainCharacterPos.x = newPos.x;
				this.mainCharacterPos.y = newPos.y;

				console.log("[current main character pos] = x " + this.mainCharacterPos.x + ", y " + this.mainCharacterPos.y + " newPos.tryCount=" + newPos.tryCount);

				// process target ground hold (eat)
				this.targetGroundProcess(this.mainCharacterPos.x, this.mainCharacterPos.y);

				// update new card hold
				this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].hold = this.mainCharacter;

				//[animation]
				// main character animation
				this.tween(this.mainCharacter).wait(1).to(
					{
						x: this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].x + 10,
						y: this.cards[this.mainCharacterPos.y][this.mainCharacterPos.x].y + 10
					}
					, 1).wait(1);

				// rotate circle animation
				that.tween(that.rotateCircle).to({
						x: that.mainCharacter.x + this.mainCharacter.width / 2,
						y: that.mainCharacter.y + this.mainCharacter.height / 2,
						rotation: -3.14 * 2
					}, 0.4, "01")
					//.to({ rotation: -3.14*2 * this.randResult/7 , scale:2}, 1).wait(1)
					.to({rotation: -3.14 * 2 + rotateAngle, scale: 2.2}, 0.8).wait(0.5)
					.to({x: this.center.x, y: this.height - 160 / 2 - 80, rotation: 0, scale: 1.4}, 1, "outBounce");

				this.actionPoint -= 1;
				this.updateActionPointBar();
			}

		}

		if(isClick(this.exitButton, data, function(){})){
			console.log('exit');
			window.location.replace('/rex/');
		}

		if(isClick(this.changeWorldButton, data, function(){})){
			// can't press if readme page is shown
			if(this.isReadmePageVisible == false) {

				if (this.changeWorldChance > 0) {
					this.changeWorldChance--;
					this.changeWorld();
				}
				//alert("press change world button");
			}
		}

		if(isClick(this.readmeButton, data, function(){})){
			this.isReadmePageVisible = true;
		}

		if(isClick(this.readmePageCloseButton, data, function(){})){
			//this.isReadmePageVisible = false;

			var that = this;
			setTimeout(function(){
				that.isReadmePageVisible = false;
			}, 200);
		}

	},
	updateActionPointBar: function(){
		//update ap
		//console.log("check point = "+this.actionPoint);
		this.tween(this.actionPotinBar).to({width:this.actionPotinBar.widthMax *this.actionPoint/this.actionPointMax, height:this.actionPotinBar.height},1);
	},

	// Target
	targetGroundProcess:function(newX, newY){
		console.log("[scoreProcess function] newX:"+newX+" newY:"+newY);
		var hold = this.cards[newY][newX].hold;

		if(hold.isAcitve == false){
			console.log('!isAcitve '+hold);
			return;
		}

		if(hold != ""){
			console.log("hold !='' ");
			if(hold.type == "police"){
				this.score += 100;
				console.log("police, score +100");
				if(this.mainCharacter.case>0){
					this.mainCharacter.case --;
					this.score += 100;
					console.log("police, score ex +100");
				}

				this.showScore();

				this.targetGroundProcessEnd(hold);
			}
			if(hold.type == "thief"){
				this.score -= 50;
				console.log("theif, score -50");
				this.showScore();

				this.targetGroundProcessEnd(hold);
			}
			if(hold.type == "case"){
				this.score += 50;
				this.mainCharacter.case += 1;
				console.log("case, score +50");
				this.showScore();

				this.targetGroundProcessEnd(hold);
			}
			if(hold.type == "ap"){
				//this.updateActionPointBar();
				this.score += 300;

				console.log("ap, score +300");
				this.showScore();

				console.log("update ap bar!!!!  old= " + this.actionPoint);


				this.targetGroundProcessEnd(hold);
			}
			if(hold.type == "cw"){
				this.score += 500;

				console.log("cw, score +500");
				this.showScore();

				this.targetGroundProcessEnd(hold);
			}
		}
	},

	// nest function !!
	targetGroundProcessEnd: function(hold){
		var that = this;
		var thatHold = hold;

		setTimeout(function(){
			thatHold.isAcitve = false;
			if(thatHold.type == "ap"){
				if(that.actionPoint>0){
					that.actionPoint += 2;
					if (that.actionPoint > that.actionPointMax) {
						console.log("ap limit <"+that.actionPointMax);
						that.actionPoint = that.actionPointMax;
					}
				}
				that.updateActionPointBar();
				console.log("update ap bar!!!!  new= "+that.actionPoint );
			}



			if(thatHold.type == "cw"){
				//that.changeWorld();
				that.changeWorldChance ++;
			}

			if(thatHold.type == "thief"){
				//alert("[Thief] You are so unlucky!");
			}
		}, 2000);

		setTimeout(function(){
			if(thatHold.type == "police"){
				var ans = prompt("[Police] What should you do?", "");
				if (ans == "999" || ans == "help") {

					alert("You got extra point!");
					that.score += 50;
					that.showScore();
				}else{
					alert("[Police] Please be careful!");
				}

			}
		},2500);

	},
	showScore: function(){
		this.tween(this.scoreFont).to({text:this.score},1).wait(1).to({size:40,x:20});
		this.tween(this.scoreFont).to({size:80, x:this.center.x - this.width/3},1, "outBounce").wait(1).to({size:40,x:20});
	},

	// Update
	step: function(dt) {
		//this.thomas.x += this.thomas.speed * dt;

		//if(this.thomas.x > this.width) this.thomas.x = -this.thomas.width;
		//this.debugText = this.thomas.x;

		if(this.actionPoint == 0 && this.isGameOver == false) {
			this.isGameOver = true;
			this.actionPotinBarText = "";
			var that = this;
			setTimeout(function(){
				alert('Game Over , your score = '+that.score);

				var playerName = prompt("Please enter your name:\n(Press cancel if you do not want to record)", "");
				if (playerName!=null) {
					var sendScore = btoa(that.score);
					window.location.replace("/rex/gamefiles/gameover.php?score="+sendScore+"&playerName="+playerName);
				}else{
					window.location.replace("/rex/ranking");
				}


			},3000);
			//this.setState(GameOver);
			return;
		}
	},

	// Render
	render: function() {
		var thomas = this.thomas;
		this.layer
			.clear("#000")
			//.drawImage(this.images.ship, thomas.x, thomas.y, thomas.width, thomas.height)
			.drawImage(this.images.bg, 0, 0, this.width, this.height)
			.font("26px Arial")
			.fillStyle("#6C9FD5")
			.fillText(this.text, 20, 60)
			.fillText(this.debugText, 20, 60*2)
			//.fillStyle(thomas.color)
			//.fillRect(thomas.x, thomas.y, 10, 10);


		// exit button
		this.layer.a(0.75).fillStyle("#fff").roundRect(this.exitButton.x, this.exitButton.y, this.exitButton.width, this.exitButton.height,12).fill().ra()
			.font("26px Arial").fillStyle("#000").fillText("Exit", this.exitButton.x+this.exitButton.width/2 -cq().textBoundaries("Exit", maxWidth = 0).width*1.5, this.exitButton.y+this.exitButton.height/2+10);

		// action point bar
		if(this.actionPoint>=0){
			//this.layer.fillStyle("#ff0000").fillRect(this.actionPotinBar.x, this.actionPotinBar.y, this.actionPotinBar.width, this.actionPotinBar.height)
			//this.layer.drawImage(this.images.actionPointBarBg, this.actionPotinBar.x, this.actionPotinBar.y, this.actionPotinBar.width, this.actionPotinBar.height)
			//var outline = cq(this.images.actionPointBar).outline();


			//this.layer.drawImage(outline.canvas, this.actionPotinBar.x, this.actionPotinBar.y, this.actionPotinBar.width, this.actionPotinBar.height)
			this.layer.drawImage(this.images.actionPointBar, this.actionPotinBar.x, this.actionPotinBar.y, this.actionPotinBar.width, this.actionPotinBar.height)
				.font("24px Arial").fillStyle("#fff").fillText(this.actionPotinBarText, this.actionPotinBar.x+this.actionPotinBar.width/2 -68, this.actionPotinBar.y+this.actionPotinBar.height/2+10);
		}

		// score
		this.layer.font(this.scoreFont.size+"px Arial").fillStyle("#fff")
			.fillText("Score: "+parseInt(this.scoreFont.text), this.scoreFont.x, this.height/5.8);

		// case text
		this.layer.font("40px Arial").fillStyle("#fff")
			.fillText("Case: "+this.mainCharacter.case, this.width - this.width/4.8, this.height/5.8);

		//all cards
		for(var i=0; i< 5;i++){
			for (var j=0; j< 5;j++){
				var card = this.cards[i][j];
				//this.layer.fillStyle("#fff").fillRect(card.x, card.y, card.width, card.height);
				this.layer.drawImage(this.images.ground, card.x, card.y, card.width, card.height);
			}
		}

		// [Character]
		// 	police
		for(var i=0;i<this.policeNum;i++){
			if(this.police[i].isAcitve) {
				//this.layer.fillStyle("#AF5959").fillRect(this.police[i].x, this.police[i].y, this.police[i].width, this.police[i].height);
				this.layer.drawImage(this.images.police, this.police[i].x, this.police[i].y, this.police[i].width, this.police[i].height);
			}
		}
		// 	thief
		for(var i=0;i<this.thiefNum;i++){
			if(this.thief[i].isAcitve) {
				//this.layer.fillStyle("#33B054").fillRect(this.thief[i].x, this.thief[i].y, this.thief[i].width, this.thief[i].height);
				this.layer.drawImage(this.images.thief, this.thief[i].x, this.thief[i].y, this.thief[i].width, this.thief[i].height);
			}
		}
		//case
		for(var i=0;i<this.caseNum;i++){
			if(this.case[i].isAcitve) {
				var color = "#3B68C1";

				if(this.case[i].caesType==1) color = "#6D94E2";
				if(this.case[i].caesType==2) color = "#133881";

				// this.layer.fillStyle(color).fillRect(this.case[i].x, this.case[i].y, this.case[i].width, this.case[i].height);
				this.layer.drawImage(this.images.case, this.case[i].x, this.case[i].y, this.case[i].width, this.case[i].height);
			}
		}
		// AP
		for(var i=0;i<this.apNum;i++){
			if(this.ap[i].isAcitve) {
				//this.layer.fillStyle("#EBF79A").fillRect(this.ap[i].x, this.ap[i].y, this.ap[i].width, this.ap[i].height);
				this.layer.drawImage(this.images.ap, this.ap[i].x, this.ap[i].y, this.ap[i].width, this.ap[i].height);
			}
		}
		// CW
		for(var i=0;i<this.cwNum;i++){
			if(this.cw[i].isAcitve) {
				//this.layer.fillStyle("#C841DC").fillRect(this.cw[i].x, this.cw[i].y, this.cw[i].width, this.cw[i].height);
				this.layer.drawImage(this.images.cw, this.cw[i].x, this.cw[i].y, this.cw[i].width, this.cw[i].height);
			}
		}
		// 	main character
		var mainCharacter = this.mainCharacter;
		//this.layer.fillStyle("#00ffff").fillRect(mainCharacter.x, mainCharacter.y, mainCharacter.width, mainCharacter.height);
		this.layer.drawImage(this.images.mainCharacter,mainCharacter.x, mainCharacter.y, mainCharacter.width, mainCharacter.height);

		// rotate circle
		var rotateCircle = this.rotateCircle;
		this.layer.save().translate(this.rotateCircle.x, this.rotateCircle.y).align(0.5, 0.5).rotate(this.rotateCircle.rotation).scale(this.rotateCircle.scale, this.rotateCircle.scale).drawImage(this.images.rotateCircle, 0, 0)
			.restore();

		this.layer.drawImage(this.images.itemBg, this.itemBg.x, this.itemBg.y, this.itemBg.width, this.itemBg.height);
		// Change World Button
		if(this.changeWorldChance > 0) {
			this.layer.drawImage(this.images.cw, this.changeWorldButton.x, this.changeWorldButton.y, this.changeWorldButton.width, this.changeWorldButton.height);
			this.layer.fillStyle("#49723d").fillText("x"+this.changeWorldChance, this.changeWorldButton.x + 60 +46, this.changeWorldButton.y +8);
		}
		// background

		// Readme Button
		this.layer.drawImage(this.images.readmeButton, this.readmeButton.x, this.readmeButton.y, this.readmeButton.width, this.readmeButton.height);
		// Readme Page
		if(this.isReadmePageVisible) {
			this.layer.drawImage(this.images.readmePage, this.readmePage.x, this.readmePage.y, this.readmePage.width, this.readmePage.height);
			this.layer.drawImage(this.images.readmePageCloseButton, this.readmePageCloseButton.x, this.readmePageCloseButton.y, this.readmePageCloseButton.width, this.readmePageCloseButton.height);
		}

	}

});

//var GameOver = {};
//
//GameOver = {
//	create: function() {
//		this.text = "Engine";
//		/* this is called when the state is entered for the very first time */
//
//	},
//
//	step: function(delta) {
//
//	},
//
//	render: function(delta) {
//
//		this.app.layer.clear("#fff")
//			.font("32px Arial")
//			.fillStyle("#fff")
//			.fillText(this.text, 16, 32);
//
//	}
//
//};

// Common function
function isClick(targetObj, clickPos, cb){
	if(clickPos.x>targetObj.x && clickPos.x< (targetObj.x + targetObj.width)
		&& clickPos.y>targetObj.y && clickPos.y< (targetObj.y + targetObj.height)){
		//console.log(targetObj.name +' x'+(targetObj.x + targetObj.width) +' > '+clickPos.x + '>'+targetObj.x);
		//console.log(targetObj.name +' y'+(targetObj.y + targetObj.height) +' > '+clickPos.y + '>'+targetObj.y);
		cb(targetObj.index);
		return true;
	}else{
		return false;
	}
}

function isClickByCenter(targetObj, clickPos, cb){
	if(clickPos.x>targetObj.x-targetObj.width/2 && clickPos.x< (targetObj.x + targetObj.width/2)
		&& clickPos.y>targetObj.y-targetObj.height/2 && clickPos.y< (targetObj.y + targetObj.height/2)){
		//console.log(targetObj.name +' x'+(targetObj.x + targetObj.width) +' > '+clickPos.x + '>'+targetObj.x);
		//console.log(targetObj.name +' y'+(targetObj.y + targetObj.height) +' > '+clickPos.y + '>'+targetObj.y);
		cb(targetObj.index);
		return true;
	}else{
		return false;
	}
}
