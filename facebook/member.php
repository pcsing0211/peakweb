<?php
session_start();
$access_token = $_SESSION['facebook_access_token'];

require_once 'sdk/src/Facebook/autoload.php';

$fb = new Facebook\Facebook([
      'app_id' => '945965162167448',
      'app_secret' => '772d9ec87f6dcbf65c905b03eb56df2e',
      'default_graph_version' => 'v2.5'
]);


//echo file_get_contents("https://graph.facebook.com/me?fields=email&access_token=$access_token");
	
try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/me?fields=id,name,email', $access_token);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

$user = $response->getGraphUser();
echo $user;

?>