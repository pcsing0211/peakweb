var rootPath = "/rex";


$( document ).on( "pageinit", function() {
  	console.log("init");
});

$( document ).on( "pagechange", function (e, data) {
	// current $.mobile page name
	var pageName = data.toPage[0].id;
	console.log("----------- Page change, current = " + pageName);
	
	// current path
	console.log("current path = " + window.location.pathname);
	
	// error handle
	try{
		if(isEnterRoot == null){
		//if(isEnterRoot == null || isEnterRoot == false){
			console.log("need to reload");
		}
	}catch(err) {
		//if(pageName != 'api' && pageName != 'api_view'){
			console.log("Please enter from root");
			localStorage.setItem("redirectUrl", window.location.pathname);
			window.location.replace("/rex/");
			
			
		//}
	}
	
	
	
	//console.log($('#'+pageName+' div[data-role="footer"]').html());
	//console.log($.mobile.activePage.find('div[data-role="footer"]').html());
	var footerObj = $.mobile.activePage.find('div[data-role="footer"]');
	
	if(localStorage.getItem("login") != null){
		 footerObj.find("#centerButton").text("Member Center");
		 footerObj.find("#centerButton").attr("href","/rex/membercenter");
		 
		 footerObj.find("#rightButton").attr("class","ui-link ui-btn ui-icon-forward registerButton ui-btn-icon-top");
		 footerObj.find("#rightButton").attr("href","/rex/logout");
		 footerObj.find("#rightButton").attr("rel","external");
		 footerObj.find("#rightButton").text("Logout");
	 
	  console.log("user");
	}else{
		footerObj.find("#centerButton").attr("href","/rex/login");
		footerObj.find("#rightButton").attr("href","/rex/register");
	  console.log("guest");  
	}
	
	if($.mobile.activePage == null){
		alert("jquery mobile down, reload page...");	
		location.reload();
	}
	
});

$( document ).on( "pageload", function(){
	console.log("load");
});



var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

// debug function
var printLogCount = 0;
var printLog = function(text, mark){
	printLogCount++;
	
	var type = "";
	if(typeof text === 'number'){
		type = "(number)";
	}else if(typeof text === 'string'){
		type = "(string)";
		if(text == "") type = "(empty string)";
	}
	if(mark == null){
		console.log('(' + printLogCount + ")\t\t" + text + "\t\t" + type);
	}else{
		console.log('(' + printLogCount + ")\t\t" + mark + ' = ' + text + "\t\t" + type);
	}
}
// end debug function


function refreshPage()
{
    jQuery.mobile.changePage(window.location.href, {
        allowSamePageTransition: true,
        transition: 'none',
        reloadPage: true
    });
}