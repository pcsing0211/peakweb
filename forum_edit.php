<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
<script src="/rex/js/common.js"></script>
</head>

<body>
<div data-role="page" id="forum_edit">
    <div data-role="header">
		<a id="backButton" href="" data-transition="slide" data-direction="reverse">Back Detail</a>
    	<h1>Edit Fraud Information</h1>
		<a id="forumButton" href="" data-transition="slide">Forum</a>
    </div>
    
	<div data-role="content">
    
        <div class="ui-field-contain"><label for="name"><strong>Topic</strong></label><input id="topic" type="text" value="topic"></div>
        <div class="ui-field-contain"><label for="name"><strong>Content</strong></label><textarea id="content" style="height:500px;">content</textarea></div>
        
        <div class="ui-block-a"><input id="submitButton" type="submit" value="Submit"><div>

    </div>
   	<div data-role="footer" data-position="fixed" class="ui-footer ui-bar-inherit ui-footer-fixed slideup">
    	<div data-role="navbar">
            <ul>
          		<li><a id="homeButton" href="/rex/" class="ui-btn-active" data-icon="home">Home</a></li>
          		<li><a id="centerButton" href="login" data-position-to="window" data-icon="arrow-u" data-transition="slideup">Login</a></li>
           		<li><a id="rightButton" href="register" data-position-to="window" data-icon="plus registerButton" data-transition="pop">Register</a></li>
            </ul>
		</div>
    </div>

<script>
$(document).ready(function(e) {
	// reading data
	var topicId = <?php echo $_GET["topicId"]; ?>;
	
	$("#forum_edit #backButton").attr("href", rootPath + "/forum/view/"+ topicId);
	$("#forum_edit #forumButton").attr("href", rootPath + "/forum");
	
	// try auto login if session exist
	if(localStorage.getItem("login") == null){
		alert("You have to login for editing!");
		//window.location.replace(rootPath + "/forum/view/" + topicId);
		$.mobile.changePage(rootPath + "/autologin");
	}
	
	var exeJson = function(cb){
    	$.getJSON(rootPath + "/program/forum/view/" + topicId, function(obj){
			//var items = '<li data-icon="false"><a href="#" class="ui-btn" data-transition="slide">' + obj["topic"] +':'+ obj["content"] +'</a></li>';
			cb(obj);
		});
	}
	
	function itemsCallback(obj){
		printLog(JSON.stringify(obj));
		$("#forum_edit #topic").val(obj["topic"]);
		$("#forum_edit #content").val(obj["content"]);
	}
	
	exeJson(itemsCallback);
	
	// submit edit data
	$("#forum_edit #submitButton").click(function(){
		var urls = rootPath + "/program/forum/edit";
          
		var id = topicId;
		var topic=$("#forum_edit #topic").val();
		var content = $("#forum_edit #content").val();
		
		var data = {topic:topic, content:content, topicId:topicId};
		$.ajax({
			url: urls,
			data: data,
			type: "PUT",
			dataType:'text',

			success: function(msg){
				printLog("edit OK, " + msg);
				var obj = JSON.parse(msg);
				if(obj["result"] == true){
					$.mobile.changePage(rootPath + "/forum/view/" + id);
				}else{
					alert(obj["reason"]);
				}
			},

			error:function(xhr, ajaxOptions, thrownError){
				printLog(xhr.status);
				printLog(thrownError);
			}
		});
	});
	
	
	
	
});

</script>
</div>

</body>
</html>
