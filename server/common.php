<?php
@session_start();

class Common{
	public static $rootPathHttp = 'http://peak.rurishiki.com/rex';
	public static function debugPrint($var){
		echo "\n(". gettype($var) . ") " . $var . "\n";
	}
	
	public static function isExist($var){
		if(gettype($var) == "integer"){
			return $var > 0;
		}else if(gettype($var) == "string"){
			return $var != "";
		}else{
			return $var != null;
		}
	}
	
	public static function echoIsExist($var){
		if(gettype($var) == "integer"){
			echo $var > 0 ? "true" : "false";
		}else if(gettype($var) == "string"){
			echo $var != "" ? "true" : "false";
		}else{
			echo $var != null ? "true" : "false";
		}
	}
	
	/*public static function getSingleValue($table, $colume){
		$result = mysql_query("select $colume from $table");
		while($row = mysql_fetch_assoc($result)){
			return $row[$colume];
		}
	}*/
	

	public static function getDBSingleValue($table, $colume, $condition){
		if(@$condition==null) $condition = "";
		$sql = "select $colume from $table $condition";
		$result = mysql_query($sql);
		//echo "($sql)<br>";
		/*while($row = mysql_fetch_assoc($result)){
			// default
			if($colume == '*') return $row['id'];
			if($colume !="" && sizeof(explode(',', $colume))>1) {
				return $row;
			}
			return $row[$colume]; // empty = ""
		}*/
		$row = mysql_fetch_assoc($result);
		return $row[$colume];
	}
	
	public static function echoJSON($bool, $json){
		if(@$json == null) $json = array();
		
		$json['result'] = $bool;
		echo json_encode($json, JSON_UNESCAPED_UNICODE);	
	}
	
	/*public static function countSingleValue(){
		
	}*/
	
	public static function isValueExistDB($table, $colume, $value){
		$result = mysql_query("select * from $table where $colume = '$value'");
		
		if($result == null) return false;
		return mysql_num_rows($result) > 0 ? true : false;
	}
	
	public static function getUserId($ac, $pw){
		$pw = md5($pw);
		$userId = Common::getDBSingleValue('member','id', "where account='$ac' and password='$pw'");
		if($userId == "" || $userId== null) return -1;
		return $userId;
	}
	
	public static function getUserIdAlreadyMd5($ac, $pw){
		$userId = Common::getDBSingleValue('member','id', "where account='$ac' and password='$pw'");
		if($userId == "" || $userId== null) return -1;
		return $userId;
	}
	
	public static function getNicknameByUserId($userId){
		return Common::getDBSingleValue('member','nickname', "where id=$userId");
		
	}
	
	public static function checkMethod($method){
		// Method: PUT
		$request_method = $_SERVER['REQUEST_METHOD'];
		if($request_method != $method){
			$json['reason'] = 'wrong-method';
			echo Common::echoJSON(false, $json);
			return false;
		}
		return true;
	}
	
	public static function checkLoginApi($ac, $pw){
		if(@$_GET['api']==true && Common::getUserId($ac,$pw) <1){
			$json['reason'] = 'ac-or-pw-wrong';
			Common::echoJSON(false, $json);
			return false;
		}
			
		return true;
			
	}
	
	public static function isApi(){
		if(@$_GET['api'] == true){
			return true;
		}else{	
		 	return false;
		}
	}
}
?>