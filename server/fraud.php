<?php
include "common.php";
include "database.php";

class fraud{
	public function allTopic(){
		
		$json = array();
		$sqlResult = mysql_query("select * from fraud");
		$i = 0;
		while($row = mysql_fetch_assoc($sqlResult)){
			//print($row["content"] . "<br>\n");
			$json[$i]["id"] = $row["id"];
			$json[$i]["topic"] = $row["topic"];
			$json[$i]["content"] = $row["content"];
			$i++;
		}
		
		/*$arr = array("a","b","c");
		$i = 0;
		$arr2 = array();
		while($i<3){
			$arr2[$i] = $arr[$i];
			$i++;
		}
		echo json_encode($arr2);*/
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
		
	}
	
	// topic/{page}/
	public function topic(){
		if(@$_GET['api'] == true){
			$json['api'] = true;
		}
		$page = $_GET["page"];
		$itemNum = 10;
		$data = array();
		if($page == 0){
			$sqlResult = mysql_query("select id, topic from fraud");
		}else{
			$sqlResult = mysql_query("select id, topic from fraud limit ".($page-1)*$itemNum.",".$itemNum);
		}
		
		//Common::debugPrint($sqlResult);
		//echo "select * from fraud limit ".($page-1).",".($page-1) * $itemNum;
		$i = 0;
		while($row = mysql_fetch_assoc($sqlResult)){
			//print($row["content"] . "<br>\n");
			$data[$i]["id"] = $row["id"];
			$data[$i]["topic"] = $row["topic"];
			//$data[$i]["content"] = $row["content"];
			$i++;
		}
		
		/*$arr = array("a","b","c");
		$i = 0;
		$arr2 = array();
		while($i<3){
			$arr2[$i] = $arr[$i];
			$i++;
		}
		echo json_encode($arr2);*/
		$json["data"] = $data;
		$json["totalPage"] = $this->getTotalPage(10);
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
		
		
	}
	
	public function getTopic(){
		$topicId = $_GET["topicId"];
		$sqlResult = mysql_query("select id, topic from fraud where id = '$topicId'");
		while($row = mysql_fetch_assoc($sqlResult)){
			$json["id"] = $row["id"];
			$json["topic"] = $row["topic"];
			//$data[$i]["content"] = $row["content"];
			break;
		}
		
		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}
	
	public function totalPage(){
		$itemsPerPage = $_GET["per"];
		$json["totalPage"] = $this->getTotalPage($itemsPerPage);
		echo json_encode($json);
	}
	
	private function getTotalPage($itemsPerPage){
		$sqlResult = mysql_query("select count(*) from fraud");
		while($row = mysql_fetch_assoc($sqlResult)){
			$count = $row["count(*)"];
			break;
		}
		return ceil($count / $itemsPerPage);
		//echo "\n<br>all item / echo page item show<br>\n";
	}
	
	public function view(){
		$data = array();
		$id = $_GET["id"];
		$sqlResult = mysql_query("select is_lock, topic, content, img, youtube, map from fraud where id = " . $id);
		while($row = mysql_fetch_assoc($sqlResult)){
			$json["topic"] = $row["topic"];
			$json["content"] = $row["content"];
			$json["youtube"] = $row["youtube"];
			$json["img"] = $row["img"];
			$json["map"] = $row["map"];
			$json["isLock"] = $row["is_lock"] == 1 ? true : false;
			break;
		}
		
		/*if($json["isLock"] == true){
			$json['reason'] = 'locked';
			Common::echoJSON(false, $json);
		}*/
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
		
	}
	
	// Method: PUT
	public function edit(){
		$method = $_SERVER['REQUEST_METHOD'];
		parse_str(file_get_contents("php://input"),$args);
		
		$id = $args["id"];
		$topic = $args["topic"];
		$content = $args["content"];
		$img = @$args["img"];
		$youtube = @$args["youtube"];
		$map = @$args["map"];
		$userId = @$_SESSION['loginId'];
		
		if(Common::getDBSingleValue('fraud', 'is_lock', "where id = '$id'")==1){
			$json['reason'] = 'lock';
			Common::echoJSON(false, $json);
			return;
		}
		
		// login api
		if(Common::checkLoginApi(@$args['ac'], @$args['pw']) == false) return;
			
		//add to author
		$newAuthorsText = "";
		$authorsText = Common::getDBSingleValue('fraud', 'authors', "where id = '$id'");
		if($authorsText == '' ){
			$json['reason'] = 'authors-not-found';
			Common::echoJSON(false, $json);
			return;
		}
		$authorsArr = explode(',',$authorsText);
		if(in_array($userId, $authorsArr) == false){
			$authorsArr[] = $userId;
		}
		$newAuthorsText = join(',', $authorsArr);
		
		if(	$method == "PUT" ){
			$sqlResult = mysql_query("update fraud set topic= '$topic', content='$content', img='$img', youtube='$youtube', map='$map', authors='$newAuthorsText' where id = '$id'");
			//$json["sql"] = "update fraud set topic= '$topic', content='$content' where id = '$id'";
			$json["result"] = true;
		}else{
			$json["reason"] = "Unexpected $method";
			$json["result"] = false;
		}
		
		//api 
		if(@$args['api'] == true){
			$json['resultUrl'] = Common::$rootPathHttp . '/fraud/view/' .$id;
		}
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
		
	}
	
	public function add(){
		$topic = $_POST["topic"];
		$content = $_POST["content"];
		$userId = @$_SESSION['loginId'];
		
		// api
		if(Common::isApi()) $json['api'] = true;

		// login api
		if(Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']) == false)
		return;
		
		$check = mysql_query("select * from fraud where topic = '$topic' ");
		if(mysql_num_rows($check) > 0){
			$json["result"] = false;
			$json["reason"] = "Topic already exist!";
		}else{
			// add item
			mysql_query("insert into fraud (topic, content, authors) values ('$topic', '$content', '$userId')");
			// get added id
			$idSqlResult = mysql_query("select id from fraud order by id desc limit 1");
			while($row = mysql_fetch_assoc($idSqlResult)){
				$idResult = $row["id"];
				break;	
			}
			$json["id"] = $idResult;
			// result url
			if(Common::isApi()) $json['resultUrl'] = Common::$rootPathHttp . "/fraud/view/". $json['id'];
			$json["result"] = true;
		}
		
	
		
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	// commit new bookmark
	public function bookmark(){
		// add one
		$newBookmark = $_POST["bookmark"];
		$userId = @$_SESSION["loginId"];
		
		// no login
		if( @$_SESSION["loginId"] == null){
			$json["result"] = false;
			echo json_encode($json, JSON_UNESCAPED_UNICODE);
			return;
		}
		
		if(Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']) == false) return ;
		
		// already bookmark
		$bookmarks = $this->getAllBookmark($userId);
		if(in_array($newBookmark, $bookmarks)){
			$json["result"] = false;
			$json["reason"] = "Already Bookmark!";
			$json["try"] = 'del';
			echo json_encode($json, JSON_UNESCAPED_UNICODE);
			return;
		}

		if(sizeof($bookmarks)>0){
			array_push($bookmarks, $newBookmark);
			$strBookmarks = join(",", $bookmarks);
		}else{
			$strBookmarks = $newBookmark;
		}
		mysql_query("update member set bookmarks='$strBookmarks' where id='$userId' ");
		$json["bookmarks"] = $strBookmarks;
		$json["result"] = true;
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	// user is bookmarked ?
	public function bookmarkStatus(){
		$json["status"] = $this->isBookmarked($_GET["topicId"]);
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	private function isBookmarked($topicId){
		$userId = @$_SESSION["loginId"];
		$bookmarks = $this->getAllBookmark($userId);
		return in_array($topicId, $bookmarks);
	}
	
	
	// return [array]
	private function getAllBookmark($userId){
		//return "sss";
		$sqlResult = mysql_query("select bookmarks from member where id = " . $userId);
		//echo "select bookmarks from member where id = " . $userId;
		if($sqlResult == null){
			return array();
		}
		
		while($row = mysql_fetch_assoc($sqlResult)){
			$bookmarks = $row["bookmarks"];
			break;
		}
		
		// [!] prevent unexpect array size = 1 after explode
		if($row["bookmarks"] == "" || $row["bookmarks"] == null){
			return array();
		}
		return explode(",", $bookmarks);
	}
	
	// json of all bookmark
	public function allBookmark(){
		
		$userId = @$_SESSION["loginId"];
		if(@$_SESSION["loginId"] == null){
			$json["result"] = false;
			echo json_encode($json, JSON_UNESCAPED_UNICODE);
			return;
		}
		
		if(Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']) == false) return;
		
		if(@$_POST['ac']!=null){
			$userId = Common::getUserId(@$_POST['ac'], @$_POST['pw']);
		}
		
		$bookmarks = $this->getAllBookmark($userId);
		//echo json_encode($bookmarks);
		$json["result"] = true;
		$json["bookmarks"] = join(",", $bookmarks);
		//$json['debug'] = Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']);
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	// post to del bookmark
	public function delBookmark(){
		$topicId = $_POST["topicId"];
		$userId = @$_SESSION["loginId"];
		
		$bookmarks = $this->getAllBookmark($userId);
		if(($key = array_search($topicId, $bookmarks)) !== false) {
			unset($bookmarks[$key]);
		}
		
		$bookmarksText = join(",", $bookmarks);
		mysql_query("update member set bookmarks='$bookmarksText' where id='$userId' ");

		$json["userId"] = $userId;
		$json["bookmarks"] = $bookmarksText;
		$json["result"] = true;
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
		
		
	}
	
	public function checkLogin(){
		if(@$_SESSION["loginId"] == null){
			$pathArray = explode("\\", __FILE__);
			$rootPath = "/" . $pathArray[sizeof($pathArray)-1];
			//echo "Location: ".$rootPath."/autologin";
			//header("Location: /");
		}
	}
	
	// show all tag of this topic
	public function tag(){
		$topicId = $_GET['topicId'];
		
		$result = mysql_query("select tags from fraud where id=$topicId");
		while($row = mysql_fetch_assoc($result)){
			$tempTags = $row['tags'];
			break;
		}
		
		// no data
		if($tempTags == null || $tempTags == ""){
			$json['reason'] = "Can't find any Tag";
			$json['result'] = false;
			echo json_encode($json, JSON_UNESCAPED_UNICODE);
			return;
		}
		
		// ok
		$topicTags = explode(",", $tempTags);
		foreach($topicTags as $tagId ){
			$tagResult = mysql_query("select id, name from tag where id=$tagId");
			while($tagRow = mysql_fetch_assoc($tagResult)){
				$tempObj = array();
				$tempObj["id"] = $tagRow['id'];
				$tempObj["name"] = $tagRow['name'];
				//if(Common::isApi()) $tempObj["resultUrl"] = Common::$rootPathHttp.'/fraud/view/'.$tagRow['id'];
				$json["data"][] = $tempObj;
			}
		}
		
		$json['result'] = true;
		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}
	
	public function author(){
		$fraudId = $_GET['fraudId'];
		$authors = Common::getDBSingleValue('fraud', 'authors', "where id=$fraudId");
		//echo 'authors = '.$authors;
		
		// must at least 1 author
		if($authors == ""){
			Common::echoJSON(false, null);
			return;
		}
		
		$authorsArray = explode(',', $authors);
		foreach($authorsArray as $aId){
			$tempObj = array();
			$tempObj['id'] = $aId;
			$tempObj['authorName'] = Common::getDBSingleValue('member', 'nickname', "where id=$aId");
			$json["data"][] = $tempObj;
		}
		Common::echoJSON(true, $json);
	}
	
}
?>