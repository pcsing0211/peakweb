<?php
include "common.php";
include "database.php";

class tag{
	public function allTag(){
		$json = array();
		$sqlResult = mysql_query("select * from tag");
		$i = 0;
		while($row = mysql_fetch_assoc($sqlResult)){
			$json[$i]["id"] = (int)$row["id"];
			$json[$i]["name"] = $row["name"];
			//$json[$i]["usageCount"] = (int)$row["usage_count"];
			$i++;
		}
		
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
		
	}
	
	public function tagTopic(){
		$tagId = $_GET["tagId"];
		
		// all topic with tag colume
		$topicResult = mysql_query("select id, tags from fraud");
		$i = 0;
		$topic = array();
		while($row = mysql_fetch_assoc($topicResult)){
			$topic[$i]["id"] = (int)$row["id"];
			$topic[$i]["tags"] = $row["tags"];
			$i++;
		}
		
		$topicCollection = array();
		foreach ($topic as &$value) {
			$tagArr = explode(",", $value["tags"]);
			if(in_array($tagId, $tagArr)){
				$topicId = $value["id"];
				//echo ("topicId = " . $value["id"]);
				$topicObj = array();
				$topicObjResult = mysql_query("select id, topic from fraud where id=$topicId");
				//echo "select id, name from fraud where id=$topicId";
				while($topicObjRow = mysql_fetch_assoc($topicObjResult)){
					$tempObj = array();
					$tempObj["id"] = $topicObjRow["id"];
					$tempObj["topic"] = $topicObjRow["topic"];
					$tempObj["usageCount"] = $this->getTagUsageCount($tagId);
					$topicCollection[] = $tempObj;
				}
				 
			}
		}
		
		$tagName = $this->getTagName($tagId);
		if( $tagName != null ) $json["tagName"] = $tagName;
		$json["result"] = true;
		$json["count"] = sizeof($topicCollection);
		$json["topics"] = $topicCollection;
		
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	private function getTagName($tagId){
		$topicResult = mysql_query("select name from tag where id = $tagId");
		while($row = mysql_fetch_assoc($topicResult)){
			return $row["name"];
		}
		
		return null;
	}
	
	// tag usage count from all fraud
	private function getTagUsageCount($tagId){
		/*$topicResult = mysql_query("select usage_count from tag where id = $tagId");
		while($row = mysql_fetch_assoc($topicResult)){
			return $row["usage_count"];
		}
		
		return null;*/
		$tagArr = array();
		
		$topicResult = mysql_query("select tags from fraud");
		while($row = mysql_fetch_assoc($topicResult)){
			//echo sizeof(explode(",",$row['tags']))."\n";
			if($row['tags'] == "") continue;
			$tagArr = array_merge($tagArr, explode(",",$row['tags']));
		}
		//echo 'tarArr = '.sizeof($tagArr);
		
		$count = 0;
		foreach($tagArr as $id){
			if((int)$id == (int)$tagId) $count++;
		}
		
		return $count;

	}
	
	// rest get -> tag count from all fraud
	public function usageCount(){
		$tagId = $_GET['tagId'];
		//echo 'id  =' .$tagId.'<br>';
		$count = $this->getTagUsageCount($tagId);
		
		$json['tagId'] = $tagId;
		$json["usageCount"] = $count;
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	// post
	public function add(){		
		$topicId = $_POST['topicId'];
		$newTagName = $_POST['newTag'];

		$tagName = Common::getDBSingleValue('tag', 'name', "where name='$newTagName'");
		$tagId = Common::getDBSingleValue('tag', 'id', "where name='$newTagName'");
		
		if( $tagName != '' ){ // found on tag table
			if($this->isExistOnFraud($tagId, $topicId)){ // found on fraud tag colume
				$json['reason'] = 'tag-and-fraud-exist';
				Common::echoJSON(false, $json);
				return;
			}
			
			// add new tag to fraud table only
			$json['type'] = 'addToFraud';
			$json['result'] = true;
			$json['usageCount'] = (int)$this->getTagUsageCount($tagId);
			$json['addTagId'] = $tagId;
			$this->addTagToFraud($topicId, $tagId);
			echo json_encode($json, JSON_UNESCAPED_UNICODE);
			return;
		}
		mysql_query("insert into tag (name) values ('$newTagName')");
		
		//add new tag to tag table and fraud table
		$newTagId = Common::getDBSingleValue('tag', 'id', 'order by id desc');
		$json['result'] = true;
		$json['usageCount'] = (int)$this->getTagUsageCount($newTagId);
		$json['addTagId'] = $newTagId;
		
		// add tag id to fraud
		$this->addTagToFraud($topicId, $newTagId);
		
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}
	
	private function addTagToFraud($topicId, $newTagId){
		$result = Common::getDBSingleValue('fraud', 'tags', "where id=$topicId");
		
		// #fix explode problem
		$tags = $result == "" ? array() : explode(',', $result);
		if(!in_array($newTagId, $tags)){
			//echo "ins in_array";
			$tags[] = $newTagId;
			$tagsText = join(',', $tags);
			//echo $tagsText;
			mysql_query("update fraud set tags = '$tagsText' where id='$topicId'");
			//echo "update fraud set tags = '$tagsText' where id='$topicId'";
		}
	}
	
	private function isExistOnFraud($addTagId, $fraudId){
		$tagsText = Common::getDBSingleValue('fraud', 'tags', "where id=$fraudId");
		if($tagsText == "") return false;
		$tags = explode(",", $tagsText);
		
		if($tagsText !="" && sizeof($tags) > 0){
			foreach($tags as $tag){
				if($tag == $addTagId) return true;
			}
		}
		
		return false;
	}
	
	// Method: DELETE
	public function delete(){
		$method = $_SERVER['REQUEST_METHOD'];
		parse_str(file_get_contents("php://input"),$args);
		
		$deleteTagId = $args['deleteTagId'];
		$fraudId = $args['fraudId'];
		
		if(Common::checkLoginApi(@$args['ac'], @$args['pw']) == false) return;
		
		// error check 
		if(Common::getDBSingleValue('fraud', 'tags', "where id=$fraudId") == ''){
			$json['reason'] = 'no-tags-on-fraud';
			Common::echoJSON(false, $json);
			return;
		}
		
		// tags
		$tagsText = Common::getDBSingleValue('fraud', 'tags', "where id=$fraudId");
		$tagsArr = explode(',', $tagsText);
		if(($key = array_search($deleteTagId, $tagsArr)) !== false) {
			unset($tagsArr[$key]);
		}
		$json['fraud-table'] = true;
		$newTagsText = join(',', $tagsArr);
		mysql_query("update fraud set tags='$newTagsText' where id=$fraudId");
		
		// check all tag, delete tag instance if not exist on any fraud
		$allTags = array();
		$allTagsResult = mysql_query("select tags from fraud");
		while($row = mysql_fetch_assoc($allTagsResult)){
			$tempTags = explode(',', $row['tags']);
			$allTags = array_merge($allTags, $tempTags);
		}
		
		$json['debug'] = $allTags;
		if(($key = array_search($deleteTagId, $allTags)) == false) {
			//unset($tagsArr[$key]);
			$json['tag-table'] = true;
			mysql_query("delete from tag where id = $deleteTagId");
		}

		
		//
		Common::echoJSON(true, $json);
	}
	
	
}
?>