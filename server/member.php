<?php
include "common.php";
include "database.php";

class member{
	public function login(){
		$json = $this->processLogin($this->isUserExistDB($_POST["ac"], md5($_POST["pw"]))); // checking
		
		//check ac api
		if(Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']) == false)
			return;
			
		if(Common::isApi())
			$json['url'] = 'http://peak.rurishiki.com/rex';
			
		echo json_encode($json);
	}
	
	public function autoLogin(){
		$json = $this->processLogin($this->isUserExistDB($_POST["ac"], $_POST["pw"])); // checking
		//if($isExist) setcookie("login",$_POST["ac"].":".md5($_POST["pw"]),time()+36000000,"/rex","rurishiki.com",1); // cookie
		
		echo json_encode($json);
	}
	
	private function processLogin($isExist){
		if($isExist){
			$json["login"] = $_POST["ac"].":".md5($_POST["pw"]).":".$this->getNicknameByAc($_POST["ac"]).":".Common::getUserId($_POST["ac"], $_POST["pw"]);
		//echo $isExist ? "true" : "false"; // to js
			$json["result"] = true;
			
			$_SESSION["loginStatus"] = true;
			$_SESSION["loginId"] = $this->getUserId($_POST["ac"]);
		}else{
			$json["result"] = false;
		}
		
		return $json;
	}
	
	public function logout(){
		$_SESSION["loginStatus"] = false;
		$_SESSION["loginId"] = null;
		/*$isExist = $this->isUserExistDB($_POST["ac"], $_POST["pw"]); // checking
		if($isExist){*/
			//setcookie($_POST["ac"],md5($_POST["pw"]),time()-3600); // del cookie
			
		/*}else{
			echo "Operation Error";
		}
		*/
		
		//echo $isExist ? "true" : "false"; // to js
		//echo "logout function";	
	}
	
	public function register(){
		// vars
		$ac = $_POST["ac"];
		$pw = md5($_POST["pw"]);
		$nickname = $_POST["nickname"];
		
		if($this->isUserExistDB($ac, $pw)){
			//echo "User Exist";
			$json["result"] = false;
			$json["reason"] = "Account already exist";
		/*}else if($this->isNicknameExistDB($_POST["nickname"])){
			$json["result"] = false;
			$json["reason"] = "Nickname already exist";*/
		}else{
			$sqlResult = mysql_query("insert into member (account, password, nickname) values ('$ac', '$pw', '$nickname')");
			//echo "insert into member (account, password) values ('$ac', '$pw')";
			$json["login"] = $_POST["ac"].":".md5($_POST["pw"]).":".$_POST["nickname"].":".Common::getUserId($_POST["ac"], $_POST["pw"]);
			
			// session
			$addedUserId = Common::getDBSingleValue('member', 'id', 'order by id desc');
			$_SESSION["loginId"] = $this->getUserId($_POST["ac"]);
			
			$json["result"] = true;
		}
		
		echo json_encode($json);
	}
	
	public function listAll() {
		//echo "listAll function";
		// Database
		$sqlResult = mysql_query("select account from member");
		$i = 0;
	 	while ($row = mysql_fetch_assoc($sqlResult)) {
			$ac = $row["account"];
 			print("Member".($i+1).": $ac<br>\n");
			$i++;
 		}
	}
	
	private function getUserId($ac){
		$sqlResult = mysql_query("select id from member where account = '$ac'");
		
	 	while ($row = mysql_fetch_assoc($sqlResult)) {
			$id = $row["id"];
			break;
		}
		return $id;
	}
	
	private function isUserExistDB($ac, $pw){
		// Database
		$sql = "select * from member where account = '$ac' and password = '$pw'";
		//echo $sql;
		$result = mysql_query($sql);
	
		if($result == null){
			return false;
		}
		// Return
		return mysql_num_rows($result) > 0 ? true : false;
		//echo mysql_num_rows($result) > 0 ? "true" : "fasle";
		
		//Common::debugPrint($sql);
		//Common::debugPrint($result);
	}
	
	private function isNicknameExistDB($nickname){
		// Database
		$sql = "select * from member where nickname = '$nickname'";
		//echo $sql;
		$result = mysql_query($sql);
	
		if($result == null){
			return false;
		}
		// Return
		return mysql_num_rows($result) > 0 ? true : false;
		//echo mysql_num_rows($result) > 0 ? "true" : "fasle";
		
		//Common::debugPrint($sql);
		//Common::debugPrint($result);
	}
	
	public function getUserTopics($ac){
		$userId = $_POST["userId"];
		$sqlResult = mysql_query("select topic from froum where id = " . $userId);
		while($row = mysql_fetch_assoc($sqlResult)){
			print($row["topic"]);
		}
	}
	
	public function getNicknameByAc($ac){
		$sqlResult = mysql_query("select nickname from member where account='$ac'");
		//echo "select nickname from member where account='$ac'";
		while ($row = mysql_fetch_assoc($sqlResult)) {
			$nickname = $row["nickname"];
			break;
		}
		return $nickname;
	}
	
	public function changePassword(){
		// must method put
		if($this->checkMethod('PUT') == false) return;
		
		// must already login
		if($this->checkLogin() == false) return;
		$userId = $_SESSION['loginId'];	
		
		// vars
		parse_str(file_get_contents("php://input"),$args);
		$oldPassword = md5($args['oldPassword']);
		$newPassword = md5($args['newPassword']);
		
		// check old password
		$result = Common::getDBSingleValue('member', 'id', "where password='$oldPassword' and id=$userId");
		if($result == ""){
			$json['reason'] = 'old-password-wrong';
			$json['pw'] = $args['oldPassword'];
			$json['debug'] = $oldPassword;
			$json['debug2'] = $newPassword;
			Common::echoJSON(false, $json);
			return;
		}
		
		mysql_query("update member set password = '$newPassword' where id=$userId");

		Common::echoJSON(true, null);
	}
	
	
	public function editNickname(){
		// must method put
		if($this->checkMethod('PUT') == false) return;
		
		// must already login
		if($this->checkLogin() == false) return;
		$userId = @$_SESSION['loginId'];
		
		
		
		// vars
		parse_str(file_get_contents("php://input"),$args);
		$newNickname = $args['newNickname'];
		
		//check ac api
		if(Common::checkLoginApi(@$args['ac'], @$args['pw']) == false){
			Common::eachJSON(false, "acOrPwIncorrect");
			return;
		}else{
			//$userId = Common::getUserId(	@$args['ac'], @$args['pw']);
			//if($userId == -1){
				
			//}
		}
		
		mysql_query("update member set nickname = '$newNickname' where id=$userId");
		//echo "update member set nickname = '$newNickname' where id=$userId";
		Common::echoJSON(true, null);
	}
	
	public function deleteAccount(){
		// must method put
		if($this->checkMethod('DELETE') == false) return;
		
		// must already login
		if($this->checkLogin() == false) return;
		$userId = $_SESSION['loginId'];	
		
		mysql_query("delete from member where id=$userId");
		$_SESSION['loginId'] = null;
		
		// delete each fraud author
		$sqlResult = mysql_query("select id, authors from fraud");
		$updateFraudAuthorsCount = 0;
		while($row = mysql_fetch_assoc($sqlResult)){
			$fraudId = $row['id'];
			
			if($row['authors'] == ''){
				continue;
			}
			
			$authorsArr = explode(',', $row['authors']);
			if(($key = array_search($userId, $authorsArr)) == true){
				unset($authorsArr[$key]);
				$updateFraudAuthorsCount++;
			}
			$newAuthorsText = join(',', $authorsArr);
			mysql_query("update fraud set authors='$newAuthorsText' where id=$fraudId");
			$json['debug'][] = "update fraud set authors='$newAuthorsText' where id=$fraudId";
		}
		$json['updateFraudAuthorsCount'] = $updateFraudAuthorsCount;
		Common::echoJSON(true, $json	);
	}
	
	// Checking methods
	private function checkMethod($method){
		// Method: PUT
		$request_method = $_SERVER['REQUEST_METHOD'];
		if($request_method != $method){
			$json['reason'] = 'wrong-method';
			echo Common::echoJSON(false, $json);
			return false;
		}
		return true;
	}
	
	private function checkLogin(){
		if(@$_SESSION['loginId'] == null){
			$json['reason'] = 'no-session';
			echo Common::echoJSON(false, $json);
			return false;
		}
		return true;
	}
	
	public function sessionLogin(){
		$ac = $_POST['ac'];
		$pw = $_POST['pw'];
		$userId = Common::getUserIdAlreadyMd5($ac, $pw);
		$_SESSION['loginId'] = $userId;
		$json['userId'] = $userId;
		Common::echoJSON(true, $json);
		
	}
	
	
}
?>